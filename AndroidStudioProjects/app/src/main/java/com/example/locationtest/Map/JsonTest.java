package com.example.locationtest.Map;

import android.app.Activity;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Scanner;

public class JsonTest extends Activity {
    String param;
// 자바에서 Post 방식으로 데이터 보내리 위해서는 URLConnection 객체 설정하기
    //setDoOutput()메소드를 true 로 설정해서 URL 을 연결하고 데이터를 기록
    //setRequestProperty() 메소드로 ContentType등을 지정해주어야 함.
    //DataoutputStream을 이용해서 URLEncoding된 문자열을 사용해야 한다.
    {
        try {
            param = "name=" + URLEncoder.encode("미니", "UTF-8");
            URL url = new URL("http://skdlkf.skd");
            URLConnection conn = url.openConnection();

            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestProperty("Content-Type", "application");

            try (DataOutputStream out = new DataOutputStream(conn.getOutputStream())) {
                out.writeBytes(param);
                out.flush();
            }
            InputStream is = conn.getInputStream();
            Scanner scan = new Scanner(is);

            int line = 1;
            while (scan.hasNext()) {
                String str = scan.nextLine();
                System.out.println((line++) + ":" + str);
            }
            scan.close();
        }catch (MalformedURLException e){
            System.out.println("The URL Address is incoreect");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.print("It can't connect to the web page.");
            e.printStackTrace();
        }
    }

}
