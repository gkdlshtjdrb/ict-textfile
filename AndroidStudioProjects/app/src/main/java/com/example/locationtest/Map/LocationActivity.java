package com.example.locationtest.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.widget.Toast;

import com.example.locationtest.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

public class LocationActivity extends AppCompatActivity {
    private static final int REQUEST_USED_PERMISSION=200; //요청에 대한 응답 코드 정의한 것

    private static final String MAP_BUNDLE_KEY="MapBundleKey";
    private MapView mapView;
    private GoogleMap map;
    private static final String[] needPermissions={
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };  //요청할 권한을 배열로 정의
    private static final LatLng DEFAULT_LOCATION=new LatLng(37.56641923090,126.9778741551);//기본 위치로 사용할 상수값, 서울
    private static final int DEFAULT_ZOOM = 15; //기본 지도 확대 값
    private Location lastKnownLocation; //구글 API 클라이언트 변수
    private static final long INTERVAL_TIME=5000; // 업데이트 주기 5초
    private static final long FASTEST_INTERVAL_TIME=2000; //가장 빠른 업데이트 주기 2초
    private int pinNumber=1;//마커 번호를 위한 변수
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);

        boolean permissionToLocationAccepted=true;

        switch (requestCode){
            case REQUEST_USED_PERMISSION:
                for(int result : grantResults){
                    if(result != PackageManager.PERMISSION_GRANTED){
                        permissionToLocationAccepted=false;
                        break;
                    }
                }
                break;
        }
        if(permissionToLocationAccepted==false){
            finish();//권한 요청이 실패 했다면 종료.
        }else{
            getMyLocation();
        }
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        for (String permission : needPermissions){//요청할 권한들이 있는지 확인
            if(ActivityCompat.checkSelfPermission(this, permission)!=PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this,needPermissions,REQUEST_USED_PERMISSION); //권한이 없다면 요청한다.
                break;
            }
        }

        setContentView(R.layout.activity_location);

        Bundle mapViewBundle = null; //저장된 MapView 상태 정보를 담을 Bundle 변수
        if(savedInstanceState!=null){ //액티비티의 상태가 저장되어 있는 경우
            mapViewBundle=savedInstanceState.getBundle(MAP_BUNDLE_KEY); //키로 맵뷰의 상태 정보 찾아서 저장
        }
        mapView=(MapView)findViewById(R.id.map);
        mapView.onCreate(mapViewBundle);//맵뷰의 onCreate메서드 호출



        mapView.getMapAsync(new OnMapReadyCallback(){
        @Override
        public void onMapReady(GoogleMap googleMap){
            map=googleMap;
/*            LatLng seoul = new LatLng(37.56641923090,126.9778741551);
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(seoul);
            markerOptions.title("서울");
            map.addMarker(markerOptions);
            map.addMarker(markerOptions);
            map.moveCamera(CameraUpdateFactory.newLatLng(seoul));//지도를 마커 위치로 이동시킴*/

            getMyLocation();

             }
         });
    }



    private void getMyLocation(){
        if (ActivityCompat.checkSelfPermission(LocationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(LocationActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED)  {
            return;
        }//내 위치 기능 사용전 권한 확인
        map.setMyLocationEnabled(true);//내 위치 마커 표시
        map.getUiSettings().setMyLocationButtonEnabled(true);//내 위치로 이동 버튼 표시

        FusedLocationProviderClient fusedLocationProviderClient = new FusedLocationProviderClient(this);
        Task<Location> task=fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>(){
            @Override
            public void onSuccess(Location location){
                lastKnownLocation=location;
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude()),
                        DEFAULT_ZOOM)); // 지도를 내 위치로 이동
                updateMyLocation();
                setMapLongClickEvent();
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (ActivityCompat.checkSelfPermission(LocationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(LocationActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED)  {
                    return;
                }
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(DEFAULT_LOCATION, DEFAULT_ZOOM));

                map.setMyLocationEnabled(false);//지도를 기본 위치로 이동
                map.getUiSettings().setMyLocationButtonEnabled(false); // 내위치표시관련UI제거
            }
        });
    }

    private void updateMyLocation(){ //위치 업데이트 메서드
        if (ActivityCompat.checkSelfPermission(LocationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(LocationActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED)  {
            return;
        }
        final LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY); //업데이트 정확도 설정
        locationRequest.setInterval(INTERVAL_TIME);//업데이트 주기 설정
        locationRequest.setFastestInterval(FASTEST_INTERVAL_TIME);//가장 빠른 업데이트 주기 설정

        FusedLocationProviderClient fusedLocationProviderClient=new FusedLocationProviderClient(this);
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, new LocationCallback(){//위치 업데이트 요청
            @Override
            public void onLocationResult(LocationResult locationResult){
                super.onLocationResult(locationResult);
                Location location=locationResult.getLastLocation();//위치정보가져옴
                map.moveCamera(CameraUpdateFactory.newLatLng(
                        new LatLng(location.getLatitude(),location.getLongitude())//위치 업데이트 시 지도를 이동시킴
                ));
            }
        },null);
    }



    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        Bundle mapBundle=outState.getBundle(MAP_BUNDLE_KEY);
        if(mapBundle==null){
            mapBundle=new Bundle();
            outState.putBundle(MAP_BUNDLE_KEY,mapBundle);
        }
        mapView.onSaveInstanceState(mapBundle);


    }

    private void setMapLongClickEvent(){
        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener(){
            @Override
            public void onMapLongClick(LatLng latLng) {
                Toast.makeText(LocationActivity.this, "위도" + latLng.latitude + "경도" + latLng.longitude, Toast.LENGTH_LONG).show();
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);//마커 위치 설정
                markerOptions.title("핀" + pinNumber);//마커 타이틀 설정
                MarkerOptions icon = markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.googleg_standard_color_18));//마커 커스텀 이미지 설정
                map.addMarker(markerOptions);
                pinNumber = pinNumber + 1;//마커 번호 증가
            }
        });
    }
    @Override
    protected void onResume(){
        super.onResume();
        mapView.onResume();
    }
    @Override
    protected void onStart(){
        super.onStart();
        mapView.onStart();
    }
    @Override
    protected void onStop(){
        super.onStop();
        mapView.onStop();
    }
    @Override
    protected void onPause(){
        super.onPause();
        mapView.onPause();
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        mapView.onDestroy();
    }
    @Override
    public void onLowMemory(){
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
