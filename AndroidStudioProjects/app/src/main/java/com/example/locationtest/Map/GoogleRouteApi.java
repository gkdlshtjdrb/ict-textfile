package com.example.locationtest.Map;

import android.content.Context;
import android.os.HandlerThread;
import android.os.Looper;

import com.example.locationtest.R;
import com.google.android.gms.maps.model.LatLng;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import android.os.Handler;
import java.util.logging.LogRecord;


import javax.net.ssl.HttpsURLConnection;

public class GoogleRouteApi extends Thread {
    public interface EventListener{//콜백리스너로 사용할 인터페이스 정의
        void onApiResult(String result);//성공시 호출할 메서드 정의
        void onApiFailed();//실패시 호출할 메서드 정의
    }
    private Handler handler;
    private EventListener listener; //LocationActivity에서 전달될 콜백 리스너를 저장할 변수
    private String apiKey;
    private LatLng startPoint;// 내 위치 정보를 저장할 변수
    private LatLng endPoint; // 사용자가 선택한 위치 정보를 저장할 변수

    public GoogleRouteApi(Context context, LatLng startPoint , LatLng endPoint, EventListener eventListener){
        this.handler= new Handler(Looper.getMainLooper());
        this.listener = eventListener;
        this.apiKey=context.getResources().getString(R.string.google_api_key);
        this.startPoint=startPoint;
        this.endPoint=endPoint;
    }
    @Override
    public void run(){
        //네트워크 조회 코드
        HttpsURLConnection httpsURLConnection=null;
        try{
            URL url = new URL("https://maps.googleapis.com/maps/api/directions/");
            httpsURLConnection =(HttpsURLConnection)url.openConnection();
            if (httpsURLConnection.getResponseCode() == HttpsURLConnection.HTTP_OK){
                InputStream inputStream = httpsURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream,"UTF-8");
                BufferedReader bufferedReader=new BufferedReader(inputStreamReader);
                String line;
                StringBuilder stringBuilder = new StringBuilder();
                while((line=bufferedReader.readLine())!=null){
                    stringBuilder.append(line);
                }
                bufferedReader.close();
                String result = stringBuilder.toString();
                onApiResult(result);
            }else{
                onApiFailed();
            }
        }catch (Exception e){
            e.printStackTrace();
            onApiFailed();
        }finally {
            if(httpsURLConnection!=null){
                httpsURLConnection.disconnect();
            }
        }
    }
    private void onApiResult(final String result){
        if(listener!=null){
            handler.post(new Runnable(){
                @Override
                public void run(){
                    listener.onApiResult(result);
                }
            });
        }
    }
    private void onApiFailed(){
        if(listener!=null){
            handler.post(new Runnable(){
                @Override
                public void run(){
                    listener.onApiFailed();
                }
            });
        }
    }
}
